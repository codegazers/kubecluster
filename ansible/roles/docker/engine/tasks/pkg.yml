---
- name: apt | Install apt-transport-https
  when: ansible_os_family == "Debian"
  apt:
    name: "apt-transport-https"
    state: present
  tags:
    - docker-engine

- name: apt | Add Docker APT GPG key
  when: ansible_os_family == "Debian"
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
  tags:
    - docker-engine

- name: apt | Add Docker APT repository
  when: ansible_os_family == "Debian"
  apt_repository:
    repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
    state: present
    filename: 'docker'
  tags:
    - docker-engine

- name: apt | Install docker engine (Debian/Ubuntu)
  when: ansible_os_family == "Debian"
  apt:
    update_cache: yes
    name: "docker-ce={{ docker_version }}"
    state: present
    force: yes
  tags:
    - docker-engine

# - name: apt | Hold docker version
#   when: ansible_os_family == "Debian"
#   dpkg_selections:
#     name: docker
#     selection: hold
#   tags:
#     - docker-engine


# Docker EE Install on RedHat

- name: rpm | Create 'dockerurl' vars file
  when:
    - ansible_os_family == "RedHat" 
    - license_key is defined
  copy:
    dest: "/etc/yum/vars/dockerurl"
    content: |
        https://storebits.docker.com/ee/linux/{{ license_key }}/centos
  tags:
    - docker-engine

- name: rpm | Install Docker repository
  when:
    - ansible_os_family == "RedHat" 
    - license_key is defined
  shell: yum-config-manager \
    --add-repo \
    "https://storebits.docker.com/ee/linux/{{ license_key }}/centos/docker-ee.repo"
  tags:
    - docker-engine

- name: rpm | Install the gpg key
  when:
    - ansible_os_family == "RedHat" 
    - license_key is defined
  rpm_key:
    state: present
    key: "https://storebits.docker.com/ee/linux/{{ license_key }}/rhel/gpg"
  tags:
    - docker-engine

- name: rpm | Install Docker EE repository
  when:
    - ansible_os_family == "RedHat" 
    - license_key is defined
  shell: yum-config-manager \
    --enable {{ docker_version }}
  tags:
    - docker-engine

- name: rpm | Get the Docker engine
  when:
    - ansible_os_family == "RedHat" 
    - license_key is defined
  yum:
    name: docker
    state: present
  tags:
    - docker-engine


# Docker CE Install on RedHat
- name: rpm | Remove any previous engine
  when:
    - ansible_os_family == "RedHat" 
    - license_key is not defined
  yum:
    name: "{{ item }}"
    state: absent
  with_items:
    - docker
    - docker-client
    - docker-client-latest
    - docker-common
    - docker-latest
    - docker-latest-logrotate
    - docker-engine
  tags:
    - docker-engine    

- name: rpm | Install Docker CE repository
  when:
    - ansible_os_family == "RedHat" 
    - license_key is not defined
  shell: yum-config-manager \
    --add-repo \
    "https://download.docker.com/linux/centos/docker-ce.repo"
  tags:
    - docker-engine

- name: rpm | Install the Docker CE engine
  when:
    - ansible_os_family == "RedHat" 
    - license_key is not defined
  yum:
    name: "{{ item }}"
    state: present
  with_items:
    - docker-ce-{{ docker_version }}
    - docker-ce-cli-{{ docker_version }}
    - containerd.io
  tags:
    - docker-engine

- name: Enable and restart Docker engine
  systemd:
     name: docker
     daemon_reload: yes
     state: started
     enabled: yes
  register: started_docker
  tags:
    - docker