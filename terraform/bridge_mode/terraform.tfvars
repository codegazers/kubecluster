infra-lab = "labs"
#Ubuntu infra-os-base = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
infra-os-base = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-2009.qcow2"
#infra-os-base = "/home/virtual/ISOs/rhcos-4.2.0-x86_64-metal-bios.raw"
private_key_path = "../../keys/provision"
kvm_pool = "labs"
kvm_bridge_interface = "bridge0"
