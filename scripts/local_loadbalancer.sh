#!/bin/bash
default_k8s_managers="192.168.200.111:6443,192.168.200.112,192.168.200.113:6443"
k8s_managers="${1:-$default_k8s_managers}"

docker rm -fv k8slb 2>/dev/null && echo "...Removed old 'k8slb' container"
echo "...Launching new 'k8slb' container with following managers: ${k8s_managers}"
docker container run -d --name k8slb \
-e K8S_MANAGERS=${k8s_managers} \
-p 6443:80 \
codegazers/k8slb
